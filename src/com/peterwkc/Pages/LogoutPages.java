package com.peterwkc.Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class LogoutPages {
	
	@FindBy(id = "welcome")
	@CacheLookup
	private WebElement welcomeUser;
	
	@FindBy(css = "#welcome-menu > ul > li:nth-child(2) > a")
	@CacheLookup
  	private WebElement logoutLink;
		
	@FindBy(id = "frmLogin")
	@CacheLookup
	private WebElement loginForm;
	
	public LogoutPages() {
	}
	
	public void logout(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.navigate().to("http://opensource.demo.orangehrmlive.com/index.php/dashboard");
		
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		welcomeUser.click();
		
		WebDriverWait logoutMenuWait = new WebDriverWait(driver, 15);
		logoutMenuWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome-menu")));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		logoutLink.click();
	
		WebDriverWait logoutWait = new WebDriverWait(driver, 15);
		logoutWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("frmLogin")));		
		Assert.assertEquals(true, loginForm.isDisplayed());
		
	}
	

}
