package com.peterwkc.Pages;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class LeavePages {

	@FindBy(id = "menu_leave_viewLeaveModule")
	private WebElement leaveMenu;
	
	@FindBy(id = "menu_leave_Configure")
	private WebElement configureMenu;
	
	@FindBy(id = "menu_leave_viewHolidayList")
	private WebElement holidayMenu;
	
	@FindBy(id = "btnAdd")
	private WebElement addBtn;
	
	@FindBy(id = "holiday_description")
	private WebElement holidayName;
	
	@FindBy(id = "holiday_date")
	private WebElement holidayDate;
	
	
	public LeavePages() {
	}
	
	
	public void navigateToLeave(WebDriver driver) {
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("http://opensource.demo.orangehrmlive.com/index.php/dashboard");
		
		// Hover
		Actions hoverAction = new Actions(driver);
		hoverAction.moveToElement(leaveMenu).moveToElement(configureMenu).moveToElement(holidayMenu);
		hoverAction.click().build().perform();
	}
	
	public void configureLeaveHoliday(WebDriver driver) {
		navigateToLeave(driver);
		
		addBtn.click();
		holidayName.sendKeys("Chinese New Year");
		//holidayDate
		
	}

}
