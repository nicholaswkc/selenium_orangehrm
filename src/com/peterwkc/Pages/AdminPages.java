package com.peterwkc.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class AdminPages {
	
	@FindBy(id = "menu_admin_viewAdminModule")
	private WebElement adminMenu;
	
	@FindBy(id = "menu_admin_UserManagement")
	private WebElement userMgntMenu;
	
	@FindBy(id = "menu_admin_viewSystemUsers")
	private WebElement usersMenu;
	
	@FindBy(id = "btnAdd")
	private WebElement addBtn;
	
	@FindBy(id = "systemUser_employeeName_empName")
	private WebElement empName;

	@FindBy(id = "systemUser_userName")
	private WebElement userName;

	@FindBy(id = "systemUser_password")
	private WebElement password;
	
	@FindBy(id = "systemUser_confirmPassword")
	private WebElement confirmPassword;
	
	@FindBy(id = "btnSave")
	private WebElement saveBtn;

	@FindBy(id = "btnDelete")
	private WebElement deleteBtn;
	
	@FindBy(id = "dialogDeleteBtn")
	private WebElement dlgDeleteBtn;
	
	@FindBy(id = "menu_admin_Job")
	private WebElement adminJob;

	@FindBy(id = "menu_admin_viewJobTitleList")
	private WebElement adminJobTitle;
	
	@FindBy(id = "btnAdd")
	private WebElement addJobTitleBtn;
	
	@FindBy(id = "jobTitle_jobTitle")
	private WebElement jobTitle;
	
	@FindBy(id = "jobTitle_jobSpec")
	private WebElement jobSpec;
	
	@FindBy(id = "btnSave")
	private WebElement saveJobTitle;
	
	
	public AdminPages() {
	}
	
	public void nagivateUserPage(WebDriver driver) {
		driver.navigate().to("http://opensource.demo.orangehrmlive.com/index.php/dashboard");
		
		// Hover
		Actions hoverAction = new Actions(driver);
		hoverAction.moveToElement(adminMenu).moveToElement(userMgntMenu).moveToElement(usersMenu);
		hoverAction.click().build().perform();
	}
	  
	public void addUser(WebDriver driver) {
		nagivateUserPage(driver);
		
		/*WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("systemUser-information")));
		
		WebElement userInfo = driver.findElement(By.id("systemUser-information"));
		Assert.assertEquals(true, userInfo.isDisplayed());*/
		
		addBtn.click();
		
		WebElement userHeading = driver.findElement(By.id("UserHeading"));
		String userHeadingText = userHeading.getText();
		Assert.assertEquals("Add User", userHeadingText);
		
		// Fill up the form
		Select userRoleDropDown = new Select(driver.findElement(By.id("systemUser_userType")));
		userRoleDropDown.selectByVisibleText("Admin");
		
		empName.sendKeys("Test User");
		userName.sendKeys("peterwkc");
		Select status = new Select(driver.findElement(By.id("systemUser_status")));
		status.selectByValue("1");
		password.sendKeys("123456");
		confirmPassword.sendKeys("123456");
		saveBtn.click();
	}
	
	public void deleteUser(WebDriver driver) {
		nagivateUserPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("systemUser-information")));
		
		WebElement userInfo = driver.findElement(By.id("systemUser-information"));
		Assert.assertEquals(true, userInfo.isDisplayed());
		
		WebElement table = driver.findElement(By.id("resultTable"));
		List<WebElement> rowList = table.findElements(By.tagName("tr"));
		
		if (rowList.size() > 2) {
			WebElement checkBox = rowList.get(2).findElement(By.xpath(".//input[@type='checkbox']"));
			checkBox.click();
			if (checkBox.isSelected()) {
				deleteBtn.click();
				
				WebDriverWait modalWait = new WebDriverWait(driver, 10);
				modalWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"deleteConfModal\"]/div[2]")));
				dlgDeleteBtn.click();
			}
			WebElement tbl = driver.findElement(By.id("resultTable"));
			List<WebElement> newRowList = tbl.findElements(By.tagName("tr"));
			Assert.assertEquals(rowList.size()-1, newRowList.size());
		}
	}
	
	public void addJobTitle(WebDriver driver) {
		driver.navigate().to("http://opensource.demo.orangehrmlive.com/index.php/admin/viewSystemUsers");
		
		// Hover
		Actions hoverAction = new Actions(driver);
		hoverAction.moveToElement(adminJob).moveToElement(adminJobTitle);
		hoverAction.click().build().perform();
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("btnAdd")));	
		addJobTitleBtn.click();
		
		// File Upload
		jobTitle.sendKeys("Automation Engineer");
		jobSpec.sendKeys("C:\\Users\\peter\\workspace\\Selenium_Opensource\\data\\importData.csv");
		saveJobTitle.click();
	}

}
