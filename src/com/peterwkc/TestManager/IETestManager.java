package com.peterwkc.TestManager;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.peterwkc.Manager.WebDriverManager;

public class IETestManager {

	private WebDriverManager webDriverManager = new WebDriverManager();
	private WebDriver driver;
	
	public IETestManager() {

	}
	
	@BeforeClass
    //@Parameters({"browser"})
    public void setupTest(/*String browser*/) throws MalformedURLException {
        System.out.println("BeforeMethod is started. " + Thread.currentThread().getId());
        // Set & Get ThreadLocal Driver with Browser
        
        webDriverManager.createDriver("ie");
        driver = webDriverManager.getDriver();
    }
	
	
	
	
	@AfterClass
	public void tearDown() throws Exception {
		System.out.println("AfterMethod is started. " + Thread.currentThread().getId());
	    webDriverManager.getDriver().quit();
	}
	 
	 
	 
	

}
