package com.peterwkc.TestManager;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.peterwkc.Manager.WebDriverManager;
import com.peterwkc.Pages.AdminPages;
import com.peterwkc.Pages.LoginPages;
import com.peterwkc.Pages.LogoutPages;

@Listeners({com.peterwkc.Listener.TestListener.class})
public class FirefoxTestManager {

	private WebDriverManager webDriverManager = new WebDriverManager();
	private WebDriver driver;
	// ============================================================================================
	// Pages Object 
	private LoginPages loginPages;
	// ============================================================================================
	
	public FirefoxTestManager() {
	}

    @BeforeClass
    //@Parameters({"browser"})
    public void setupTest(/*String browser*/) throws MalformedURLException {
        System.out.println("BeforeMethod is started. " + Thread.currentThread().getId());
        
        webDriverManager.createDriver("firefox");
        driver = webDriverManager.getDriver();
    }
	
    @DataProvider(name = "loginCredentials")
    public static Object[][] getLoginCredentials() {
    	return new Object [][] {{ "Admin123", "admin123"  }, {"testUser", "test"}, {"test", "test"}};
    }
    
    /*@Test(groups= {"Login"}, description="Invalid Login", priority = 0, dataProvider = "loginCredentials", invocationCount = 3) 
    public void login_invalid(String username, String password) {
    	loginPages = PageFactory.initElements(driver, LoginPages.class);
    	loginPages.login_invalid(driver, username, password);
    }
    
    @Test(description="Valid Login", priority = 2)
    public void login() {
    	loginPages = PageFactory.initElements(driver, LoginPages.class);
    	loginPages.login(driver);
    }*/
    
    
    
    @AfterClass
    public void tearDown() throws Exception {
        System.out.println("AfterMethod is started. " + Thread.currentThread().getId());
        webDriverManager.getDriver().quit();
    }

}
