package com.peterwkc.TestManager;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.peterwkc.Manager.WebDriverManager;
import com.peterwkc.Pages.AdminPages;
import com.peterwkc.Pages.LoginPages;
import com.peterwkc.Pages.LogoutPages;

@Listeners({com.peterwkc.Listener.TestListener.class})
public class ChromeTestManager {
	
	private WebDriverManager webDriverManager = new WebDriverManager();
	private WebDriver driver;
	
	private ITestContext testCtxt; 
	// ============================================================================================
	// Pages Object 
	
	private LoginPages loginPages;
	private LogoutPages logoutPages;
	private AdminPages adminPages;
	// ============================================================================================
	
	public ChromeTestManager() {
	}
	
    @BeforeClass
    //@Parameters({"browser"})
    public void setupTest(/*String browser*/ ITestContext context) throws MalformedURLException {
        System.out.println("BeforeSuite is started. " + Thread.currentThread().getId());
        
        webDriverManager.createDriver("chrome");
        driver = webDriverManager.getDriver();
          
        testCtxt = context;
        testCtxt.setAttribute("driver", driver);
    }
    
    @DataProvider(name = "loginCredentials")
    public static Object[][] getLoginCredentials() {
    	return new Object [][] {{ "Admin123", "admin123"  }, {"testUser", "test"}, {"test", "test"}};
    }
    
    /*@Test(groups= {"Login"}, description="Invalid Login", priority = 0, dataProvider = "loginCredentials", invocationCount = 1) 
    public void login_invalid(String username, String password) {
    	loginPages = PageFactory.initElements(driver, LoginPages.class);
    	loginPages.login_invalid(driver, username, password);
    }*/
    
    @Test(groups= {"Login"}, description="Valid Login", priority = 1)
    public void login() {
    	loginPages = PageFactory.initElements(driver, LoginPages.class);
    	loginPages.login(driver);
    }
    
    @Test(groups= {"Admin"}, description="Add User", priority = 2)
    public void addUser() {
    	adminPages = PageFactory.initElements(driver, AdminPages.class);
    	adminPages.addUser(driver);
    }
    
    /*@Test(groups= {"Admin"}, description="Delete User", priority = 2)
    public void deleteUser() {
    	adminPages = PageFactory.initElements(driver, AdminPages.class);
    	adminPages.deleteUser(driver);
    }
    
    @Test(groups= {"Admin"}, description="Add Job Title", priority = 2)
    public void addJobTitle() {
    	adminPages = PageFactory.initElements(driver, AdminPages.class);
    	adminPages.addJobTitle(driver);
    }*/
    
    
    
    
    /*@Test(description="Valid Logout")
    public void logout() {
    	logoutPages.logout(driver);
    }*/

    @AfterClass
    public void tearDown() throws Exception {
        System.out.println("AfterMethod is started. " + Thread.currentThread().getId());
        webDriverManager.getDriver().quit();
    }
	

}

// ===============================================================================================

/*
 * Hub 
 * java -Dwebdriver.gecko.driver="C:\Users\peter\workspace\Selenium_grid\jar\geckodriver.exe" -jar selenium-server-standalone-3.12.0.jar -role hub
 * -port 4444
 * 
 * Node
 * java -Dwebdriver.gecko.driver="C:\Users\peter\workspace\Selenium_grid\jar\geckodriver.exe" -jar selenium-server-standalone-3.12.0.jar -role node -hub http://localhost:4444/grid/register
 * 
 * -Dwebdriver.chrome.driver="" 
 * 
 * Chrome
 * -browser "browserName=chrome, maxInstances=10, version=ANY
 * 
 * Firefox	
 * -browser "browserName=firefox, maxInstances=10, seleniumProtocol=WebDriver"
 * 
 * 
 * Node with 3 browsers
 * 
 * http://localhost:4444/grid/console
 * 
 * https://developers.perfectomobile.com/pages/viewpage.action?pageId=21435360
 * 
 * 
 */

// =======================================================================================================

/* Run TestNG from Command Line
 * java -cp ".\bin\;.\jar\*;" org.testng.TestNG testng.xml
 * 
 * 
 * 
 */
// =======================================================================================================
/*
 * Await Library
 * https://github.com/awaitility/awaitility/wiki/Usage
 * 
 * 
 */

// ================================================================================================

/*
 * http://arquillian.org/arquillian-graphene/
 * Extension to Selenium Web driver
 * 
 */

// ================================================================================================

/*
 * Properties Library
 * http://owner.aeonbits.org/
 */















