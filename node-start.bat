@echo on
title Selenium Node 1 - Opensource HRM

cd "C:\Users\peter\workspace\Selenium_Opensource"

java -Dwebdriver.gecko.driver="C:\Users\peter\workspace\Selenium_Opensource\jar\geckodriver.exe" -Dwebdriver.chrome.driver="C:\Users\peter\workspace\Selenium_Opensource\jar\chromedriver.exe" -Dwebdriver.ie.driver="C:\Users\peter\workspace\Selenium_Opensource\jar\IEDriverServer.exe" -jar .\jar\selenium-server-standalone-3.12.0.jar -role node -hub http://localhost:4444/grid/register -browser "browserName=firefox, maxInstances=10, platform=ANY, seleniumProtocol=WebDriver" -browser "browserName=internet explorer, version=11, platform=WINDOWS, maxInstances=10" -browser "browserName=chrome,version=ANY,maxInstances=10,platform=WINDOWS"




pause